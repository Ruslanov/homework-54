import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.css']
})
export class SquareComponent {
  @Input() ring = '';
  className = 'black';

  getClassName(){
    return ' square ' + this.className;
  }

  openSquare(){
    this.className = 'white'
  }
}
