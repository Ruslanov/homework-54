import {Component} from '@angular/core';
import {Cell} from "./cell";

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.css']
})
export class GameBoardComponent {
  arrayCell: Cell[] = [];
  tries = 0;
  random = 0;
  win = false;
  hasItem = false;

  constructor() {
    this.getBoard()
  }

  getBoard() {
    for (let i = 0; i < 36; i++) {
      const cell = new Cell();
      this.arrayCell.push(cell);
    }
    this.random = Math.floor(Math.random() * this.arrayCell.length);
    this.arrayCell[this.random].hasItem = true;
  }

  class() {
    if (this.win) {
      return 'board disabled';
    } else {
      return 'board';
    }
  }

  hasItems(i: number) {
    if (this.arrayCell[i].hasItem) {
      return 'O';
    } else {
      return '';
    }
  }

  getRing(i: number, event: Event) {
    const target = <HTMLDivElement>event.target;
    if (!this.arrayCell[i].click) {
      this.tries++;
      this.arrayCell[i].click = true;
      if (target.innerText === 'O') {
        alert('Вы нашли элемент с ' + this.tries + 'й попытки');
      }
    }
    if (this.arrayCell[i].hasItem) {
      this.win = true;
      return;
    }
  }

  reset() {
    this.arrayCell.splice(0, this.arrayCell.length);
    this.tries = 0;
    this.win = false;
    this.getBoard();
  }

}
